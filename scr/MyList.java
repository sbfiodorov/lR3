import java.util.Arrays;

public class MyList<T extends Comparable<T>>
{
    int length = 0;
    int capacity = 10;
    private Object[] arr = new Object[capacity];

    private void extend()
    {
        capacity += 10;
        arr = Arrays.copyOf(arr, capacity);
    }

    public void add(T elem)
    {
        if (length == capacity)
        {
            extend();
        }
        arr[length] = elem;
        length++;
    }

    public int size()
    {
        return length;
    }

    public boolean contains(T key)
    {
        for (int i = 0; i < length; i++)
        {
            if (((T) arr[i]).compareTo(key) == 0)
            {
                return true;
            }
        }
        return false;
    }

    public int indexOf(T key)
    {
        for (int i = 0; i < length; i++)
        {
            if (((T) arr[i]).compareTo(key) == 0)
                return i;
        }
        return -1;
    }

    @Override
    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("[");
        for (int i = 0; i < length - 1; i++)
        {
            str.append(arr[i]).append(", ");
        }
        str.append(arr[length - 1]).append("]");
        return str.toString();
    }
}