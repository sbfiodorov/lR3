import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class AlgorithmTest
{
    String[] data1 =
    {
            "Санкт-Петербург", "Москва", "10", "20",
            "Москва","Хабаровск","40", "35",
            "Санкт-Петербург", "Хабаровск", "14", "N/A",
            "Владивосток", "Хабаровск", "13", "8",
            "Владивосток", "Санкт-Петербург", "N/A", "20"
    };

    int[][] result1 =
    {
            {0, 10, 14, 20},
            {20, 0, 34, 40},
            {55, 35, 0, 8},
            {68, 48, 13, 0}
    };

    @Test
    public void test()
    {
        Assertions.assertArrayEquals(result1, new Algorithm(data1).run());
    }
}