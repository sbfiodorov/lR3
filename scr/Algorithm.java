import java.util.Arrays;

public class Algorithm
{
    private static String[] data;
    private static int[][] parts;
    private static final MyList<String> peaks = new MyList<>();

    public Algorithm(String[] data)
    {
        Algorithm.data = data;
    }

    public int[][] run()
    {
        getPeaks();
        getParts();
        int[][] ints = new int[peaks.size()][peaks.size()];

        for (int i = 0; i < ints.length; i++)
        {
            for (int j = 0; j < ints[i].length; j++)
            {
                if (i == j)
                {
                    ints[i][j] = 0;
                }
                else
                {
                    ints[i][j] = 10000;
                }
            }
        }

        for (int i = 0; i < ints.length; i++)
        {
            for (int j = 0; j < peaks.size() - 1; j++)
            {
                for (int[] edge : parts)
                {
                    int from = ints[i][edge[0]];
                    int to = ints[i][edge[1]];
                    int w = edge[2];
                    if (from + w < to)
                    {
                        ints[i][edge[1]] = from + w;
                    }
                }
            }
        }
        return ints;
    }

    private void getPeaks()
    {
        for (int i = 0; i < data.length; i++)
        {
            if (i % 4 == 0 || i % 4 == 1)
            {
                if (!peaks.contains(data[i]))
                {
                    peaks.add(data[i]);
                }
            }
        }

    }

    private void getParts()
    {
        parts = new int[data.length / 2][3];
        int partsNumber = 0;

        for (int i = 0; i < data.length; i++)
        {
            if (i % 4 == 2)
            {
                try
                {
                    parts[partsNumber][0] = peaks.indexOf(data[i - 2]);
                    parts[partsNumber][1] = peaks.indexOf(data[i - 1]);
                    parts[partsNumber][2] = Integer.parseInt(data[i]);
                    partsNumber++;
                }
                catch
                (NumberFormatException e)
                {
                    System.out.println();
                }

            }
            else if (i % 4 == 3)
            {
                try
                {
                    parts[partsNumber][0] = peaks.indexOf(data[i - 2]);
                    parts[partsNumber][1] = peaks.indexOf(data[i - 3]);
                    parts[partsNumber][2] = Integer.parseInt(data[i]);
                    partsNumber++;
                }
                catch (NumberFormatException e)
                {
                    System.out.println();
                }
            }
        }
        parts = Arrays.copyOfRange(parts, 0, partsNumber);
    }

    public void printMatrix()
    {
        int[][] result = new Algorithm(data).run();
        System.out.println("Матрица смежности : ");

        for (int[] ints : result)
        {
            for (int anInt : ints)
            {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
    }
}